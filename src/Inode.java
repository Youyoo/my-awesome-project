import java.util.*;

public class Inode {
    public static void main(String[] args) {
        //TODO 测试用
    }

    private static final ArrayList<Inode> INODES = new ArrayList<>();
    private static final LinkedList<Inode> IDLE_INODES = new LinkedList();
    private static final HashMap<String, Integer> INODE_INDEX = new HashMap<>();
    private static Inode ROOT;
    private static Inode CUR;

    public static final void init() {
        Block.init();
        INODES.clear();
        IDLE_INODES.clear();
        INODE_INDEX.clear();
        for (int i = 0; i < Ext4.INODE_MAX; i++) {
            Inode inode = new Inode(i);
            INODES.add(inode);
            IDLE_INODES.add(inode);
        }
        ROOT = nextIdle();
        Block block = Block.nextIdle();
        block.putInode("..", ROOT.id);
        block.putInode(".", ROOT.id);
        INODE_INDEX.put("/", ROOT.id);
        CUR = ROOT;
        System.out.println("Inode 格式化完毕");
    }

    private static final Inode nextIdle() {
        if (IDLE_INODES.size() == 0) {
            return null;
        }
        return IDLE_INODES.pop();
    }

    private static final void addIdle(Inode inode) {
        IDLE_INODES.add(inode);
    }

    private static final Inode getInode(int id) {
        if (id < Ext4.INODE_MAX) {
            return INODES.get(1);
        }
        return null;
    }

    private static final Inode getInode(String dirname) {
        Integer id;
        if (dirname.startsWith("/")) {
            id = INODE_INDEX.get(dirname);
        } else {
            Block block = CUR.get(0);
            id = block.getInode(dirname);
        }
        if (id != null) {
            return INODES.get(id);
        } else {
            return null;
        }
    }

    private int id;

    private int size;
    private int blocks=1;
    private int ioblocks;

    private String device = "801h/2049d";
    private int inode = id;
    private int links=0;

    private String auth="-rw-rw-r--";
    private String uid = "1000/ name";
    private String gid = "1000/ name";

    private Date access;
    private Date modify;
    private Date change;

    private Block[] direct = new Block[10];
    private Block singleindirect;
    private Block doubleindirect;
    private Block tripleindirect;

    private Inode(int id) {
        this.id = id;
    }

    //TODO 1
    private boolean add(Block block) {
        return true;
    }

    //TODO 2
    private boolean add(Byte[] bytes) {
        return true;
    }

    //TODO 3
    private Block get(int index) {
        return null;
    }

    //TODO 4
    private ArrayList<Block> get(int offset, int length) {
        return null;
    }

    public static final void cd(String name) {
        Inode inode = getInode(name);
        if (inode != null) {
            CUR = inode;
        }
    }

    //TODO 5
    public static final void mkdir(String name) {

    }

    //TODO 6
    public static final void mkdir_p(String name) {

    }

    //TODO 7
    public static void touch(String filename, int size) {
    }

    //TODO 8
    public static void rm(String name) {
    }

    //TODO 9
    public static void rm_r(String name) {
    }

    //TODO 10
    public static void cp(String file1, String file2) {
    }

    //TODO 11
    public static void cp_r(String dirname1, String dirname2) {
    }

    //TODO 12
    public static void mv(String filename1, String filename2) {
    }

    //TODO 13
    public static void stat(String... dirnames) {
        String name;
        if (dirnames.length == 0) {
            name = ".";
        } else if (dirnames.length == 1) {
            name = dirnames[0];
            //TODO
        } else {
            //TODO ERROR
        }
    }

    //TODO 14
    public static void ls(String... dirnames) {
        String name;
        if (dirnames.length == 0) {
            name = ".";
        } else if (dirnames.length == 1) {
            name = dirnames[0];
            //TODO
        } else {
            //TODO ERROR
        }
    }

    //TODO 15
    public static void ls_i(String dirname) {
    }

    //TODO 16
    public static void access(String filename, int offset, int length) {
    }

    public static void access(String filename) {
        Inode inode = getInode(filename);
        access(filename, 0, inode.size);
    }

    //TODO 17
    public static void used() {
    }
}

class Block {
    private static final LinkedList<Block> IDLE_BLOCKS = new LinkedList();
    private static final ArrayList<Block> BLOCKS = new ArrayList<>();

    public static final void init() {
        for (int i = 0; i < Ext4.BLOCK_MAX; i++) {
            Block block = new Block(i);
            BLOCKS.add(block);
            IDLE_BLOCKS.add(block);
        }
        System.out.println("Block 格式化完毕");
    }

    static final Block nextIdle() {
        if (IDLE_BLOCKS.size() == 0) {
            return null;
        }
        return IDLE_BLOCKS.pop();
    }

    static final void addIdle(Block block) {
        IDLE_BLOCKS.add(block);
    }

    private int id;

    private Block(int id) {
        this.id = id;
    }

    /* 如果为间接块 */
    private ArrayList<Block> blockindex = new ArrayList<>();

    boolean putBlock(Block block) {
        if (blockindex.size() < Ext4.BLOCK_MAX_INDEXS) {
            return blockindex.add(block);
        }
        return false;
    }

    Block getBlock(int index) {
        if (index < blockindex.size()) {
            return blockindex.get(index);
        }
        return null;
    }

    /* 如果为目录块 */
    private HashMap<String, Integer> inodeindex = new HashMap<>();

    void putInode(String name, int id) {
        inodeindex.put(name, id);
    }

    // 必须为 Integer
    Integer getInode(String name) {
        return inodeindex.get(name);
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
