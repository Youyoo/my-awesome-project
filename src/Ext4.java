public class Ext4 {
    public static void main(String[] args) {
        Ext4 ext4 = new Ext4();
        ext4.stat();
    }

    public static final long INODE_MAX = 65536;
    public static final long BLOCK_MAX = 67043328;
    public static final int BLOCK_MAX_BYTES = 4 * 1024;
    public static final int BLOCK_MAX_INDEXS = 512;

    static {
        Inode.init();
        System.out.println("Ext 4 格式化完毕");
    }

    public void cd(String dirname) {
        Inode.cd(dirname);
    }

    public void mkdir(String... dirnames) {
        for (String dirname : dirnames) {
            Inode.mkdir_p(dirname);
        }
    }

    public void mkdir_p(String... dirnames) {
        for (String dirname : dirnames) {
            Inode.mkdir_p(dirname);
        }
    }

    public void touch(String filename, int size) {
        Inode.touch(filename, size);
    }

    public void rm(String... names) {
        for (String name : names) {
            Inode.rm(name);
        }
    }

    public void rm_r(String... names) {
        for (String name : names) {
            Inode.rm_r(name);
        }
    }

    public void cp(String filename1, String filename2) {
        Inode.cp(filename1, filename2);
    }

    public void cp_r(String dirname1, String dirname2) {
            Inode.cp_r(dirname1,dirname2);
    }

    public void mv(String filename1, String filename2) {
        Inode.mv(filename1, filename2);
    }

    public void stat(String... dirnames) {
        Inode.stat(dirnames);
    }

    public void ls(String... dirnames) {
        Inode.ls(dirnames);
    }

    public void ls_i(String dirname) {
        Inode.ls_i(dirname);
    }

    public void access(String filename) {
        Inode.access(filename);
    }

    public void access(String filename, int offset, int length) {
        Inode.access(filename, offset, length);
    }

    public void used() {
        Inode.used();
    }
}
